#define _GNU_SOURCE

#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <stdint.h>

typedef char* str;
typedef const char* const_str;

#define by_struct_array(type) struct by_array_##type { type* dat; int len; }
#define by_array(type, ...) (type[]) { __VA_ARGS__ __VA_OPT__(,) 0 }
#define by_array_iter(type, it, begin) for(type *it = begin; *it; ++it)

#define by_strcat(...) __by_strcat(by_array(const_str, __VA_ARGS__))
str __by_strcat(const_str* strings) {
	uint32_t
		dim = 0,
		len = 0;
	str output = NULL;
	by_array_iter(const_str, s, strings) {
		const_str string = *s;
		uint32_t str_len = strlen(string);
		if (str_len+len > dim) {
			dim += str_len;
			output = realloc(output, dim+1);
		}
		*(str)mempcpy(&output[len], string, str_len) = '\0';
		len += str_len;
	}
	return output;
}

struct by_string {
	str
		dat;
	uint32_t
		len, dim;
};

struct by_string by_string() {
	uint32_t dim = 1024;
	return (struct by_string) {
		.dat = malloc(dim),
		.len = 0,
		.dim = dim,
	};
}

void by_string_delete(struct by_string *string) {
	free(string->dat);
	*string = (struct by_string) {0};
}

#define by_string_concat(dst, ...) __by_string_concat(dst, by_array(const_str, __VA_ARGS__))
void __by_string_concat(struct by_string *dst, const_str* sources) {
	by_array_iter(const_str, srcs, sources) {
		const_str src = *srcs;
		uint32_t src_len = strlen(src);
		if (dst->dim+src_len > dst->dim) {
			dst->dim += src_len;
			dst->dat = (str)realloc(dst->dat, dst->dim+1);
		}
		*(str)mempcpy(&dst->dat[dst->len], src, src_len) = '\0';
		dst->len += src_len;
	}
}

#ifndef BUILD_PATH
	#define BUILD_PATH "./build/"
#endif

// Code Compiler
#ifndef CC
	#define CC "gcc"
#endif

// Linker eDitor
#ifndef LD
	#define LD "gcc"
#endif

#define by_cmd(...) ({\
	str cmd = by_strcat(__VA_ARGS__);\
	printf("%s\n", cmd);\
	int err = system(cmd);\
	free(cmd);\
	err;\
})

time_t by_source_last_modification = {0};
bool by_rebuild_all = false;

#define build_yourself(argc, argv) assert(argc >= 1); __build_yourself(__FILE__, argv[0])
void __build_yourself(const_str source_path, const_str binary_path) {
	int err;
	struct stat
		binary_st = {0},
		source_st = {0};
	stat(source_path, &source_st);
	stat(binary_path, &binary_st);
	by_source_last_modification = source_st.st_mtime;
	if (source_st.st_mtime <= binary_st.st_mtime) return;
	printf("[Rebuilding]: ");

	str old_bin = by_strcat(binary_path, ".old");
	err = rename(binary_path, old_bin);
	if (err) {
		printf("Failed, could not rename the binary\n");
		goto defer;
	}

	err = by_cmd("gcc -o ", binary_path, " ", source_path);
	if (err) {
		printf("Failed, could not build the file\n");
		rename(old_bin, binary_path);
		goto defer;
	}

	printf("Succesful\n");
	system(binary_path);

	remove(old_bin);
defer:
	free(old_bin);
	exit(err);
}

struct by_module_config {
	const_str
		name,
		cflags,
		lflags;
};

struct by_source_dependencies {
	const_str
		*sources,
		*files;
};

enum by_status {
	BY_STATUS_PENDING,
	BY_STATUS_COMPILED,
	BY_STATUS_CHANGENT,
};

#define by_struct_vector(type, ...) struct by_vector_##type { __VA_ARGS__ type *dat; int len, dim; }

#define by_vector(type, ...) (struct by_vector_##type) {\
	malloc(sizeof(__VA_ARGS__ type)*32),\
	32,\
	0\
}

by_struct_array(const_str);
by_struct_array(int);
#define FIELDS\
	X(src, const_str)\
	X(obj, str)\
	X(dep, struct by_array_int)\
	X(fil, struct by_array_const_str)\
	X(stt, enum by_status)

struct by_module {
	struct by_module_config
		cfg;
	int
		lsp,
		len,
		dim;
	#define X(name, type) type* name;
		FIELDS
	#undef X
};

struct by_module by_module(struct by_module_config cfg) {
	int n = 32;
	return (struct by_module) {
		.cfg = cfg,
		.lsp = 0xf,
		.len = 0,
		.dim = n,
		#define X(name, type) .name = malloc(n*sizeof(type)),
			FIELDS
		#undef X
	};
}

void by_module_delete(struct by_module *module) {
	for (int i = 0; i < module->len; i++) {
		free((void*)module->obj[i]);
	}

	#define X(name, type) free(module->name);
		FIELDS
	#undef X

	*module = (struct by_module) {0};
}

int by_module_add_source(
	struct by_module*  module,
	const_str          source,
	int nd, int        dep[nd],
	int nf, const_str  fil[nf]
) {
	if (module->len >= module->dim) {
		module->dim *= 2;

		#define X(name, type) module->name = realloc(module->name, module->dim*sizeof(type));
			FIELDS
		#undef X
	}

	int id = module->len++;

	int len = strlen(source);
	if (len > module->lsp) module->lsp = len;

	module->src[id] = source;
	module->obj[id] = by_strcat(BUILD_PATH, source, ".o");
	module->dep[id] = (struct by_array_int) { dep, nd };
	module->fil[id] = (struct by_array_const_str) { fil, nf };
	module->stt[id] = BY_STATUS_PENDING;

	return id;
}

str by_get_build_path(int file_path_len, const char file_path[file_path_len]) {
	int build_path_len = sizeof(BUILD_PATH)-1;
	for (const char* c = &file_path[file_path_len]; c != file_path; --c) {
		if (*c == '/') {
			int32_t path_len = c - file_path;
			char* path = malloc(build_path_len + path_len + 1);
			memcpy(path, BUILD_PATH, build_path_len);
			*(char*)mempcpy(&path[build_path_len], file_path, path_len) = '\0';
			return path;
		}
	}
	return NULL;
}

void by_source_build(struct by_module *module, int id) {
	if (module->stt[id] != BY_STATUS_PENDING) return;
	const char
		*source_file = module->src[id],
		*object_file = module->obj[id];
	char
		*build_path = by_get_build_path(strlen(source_file), source_file);

	struct stat st;
	if (stat(build_path, &st) == -1) {
		printf("[Creating %s]: ", build_path);
		by_cmd("mkdir -p ", build_path);
	}
	free(build_path);

	printf("[Module: %s]: %-*s", module->cfg.name, module->lsp, source_file);

	struct stat
		st_src = {0},
		st_out = {0};
	stat(source_file, &st_src);
	stat(object_file, &st_out);
	bool rebuild = by_rebuild_all;

	{ // Whatch any change by part of the dependencies of dependent files
		struct by_array_int* dep = &module->dep[id];
		for (int i = 0; i < dep->len && !rebuild; i++) {
			rebuild |= module->stt[dep->dat[i]] == BY_STATUS_COMPILED;
		}

		struct by_array_const_str* fil = &module->fil[id];
		for (int i = 0; i < fil->len && !rebuild; i++) {
			struct stat st_file = {0};
			stat(fil->dat[i], &st_file);
			rebuild |= st_file.st_mtime > st_out.st_mtime;
		}
	}

	if (!(st_src.st_mtime > st_out.st_mtime || rebuild)) {
		printf("No change\n");
		module->stt[id] = BY_STATUS_CHANGENT;
		return;
	}
	module->stt[id] = BY_STATUS_COMPILED;

	{
		printf("Compiled: ");
		int err = by_cmd((CC" "), module->cfg.cflags, " -c ", source_file, " -o ", object_file);
		if (err) {
			printf("[Failed]: %d\n", err);
			exit(err);
		}
	}
}

void by_module_build(struct by_module *module) {
	char* bin_file = by_strcat(BUILD_PATH, module->cfg.name);
	struct stat bin_st = {0};
	stat(bin_file, &bin_st);

	if (by_source_last_modification > bin_st.st_mtime)
		by_rebuild_all = true;

	for (int id = 0; id < module->len; ++id) {
		int len = strlen(module->src[id])+3;
		if (module->lsp < len) module->lsp = len;
	}

	struct by_string output_list = by_string();
	for (int id = 0; id < module->len; ++id) {
		by_source_build(module, id);
		by_string_concat(&output_list, module->obj[id], " ");
	}

	bool rebuild_module = by_rebuild_all;
	for (int id = 0; id < module->len && !rebuild_module; ++id)
		rebuild_module |= module->stt[id] == BY_STATUS_COMPILED;

	printf("[Module: %s]: ", module->cfg.name);
	printf("%-*s", module->lsp, module->cfg.name);
	if (!rebuild_module) {
		printf("No change\n");
		free(bin_file);
		free(output_list.dat);
		return;
	}

	printf("Compiled: ");
	int err = by_cmd((LD" "), module->cfg.lflags, " -o ", bin_file, " ", output_list.dat);
	free(bin_file);
	by_string_delete(&output_list);
	if (err) {
		printf("[Failed]: %d\n", err);
		exit(-1);
	}
}
