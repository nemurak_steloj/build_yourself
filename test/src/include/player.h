#pragma once
#include <SFML/Graphics.h>

struct player {
	sfCircleShape *body;
	sfVector2f velocity;
};

struct player player(sfVector2f velocity);
void player_update(struct player *p);
void player_render(struct player *p, sfRenderWindow *w);
