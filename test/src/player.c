#include "player.h"
#include <SFML/Graphics/CircleShape.h>
#include <SFML/Graphics/Sprite.h>

struct player player(sfVector2f velocity) {
	sfCircleShape *body = sfCircleShape_create();
	sfCircleShape_setRadius(body, 25.f);
	sfCircleShape_setFillColor(body, sfRed);
	return (struct player) { body, velocity };
}

void player_update(struct player *p) {
	sfCircleShape_move(p->body, p->velocity);
}

void player_render(struct player *p, sfRenderWindow *w) {
	sfRenderWindow_drawCircleShape(w, p->body, NULL);
}
