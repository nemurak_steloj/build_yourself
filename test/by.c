#include "../by.h"

int main(int argc, char** argv) {
	build_yourself(argc, argv);

	const_str
		cflags =
			"-Isrc/include " // Includes
			"-Wall -Wextra " // Warnings
			"-g -xc ",       // Debug and Languaje
		lflags =
			"-lraylib "
			"-lGL "
			"-lm "
			"-lpthread "
			"-lrt "
			"-lX11 "
	;

	struct by_module module = by_module((struct by_module_config) {
		.name = "game",
		cflags,
		lflags,
	});

	by_module_add_source(&module, "src/main.c",
		0, NULL,
		0, NULL
	);

	by_module_build(&module);
	by_module_delete(&module);
	return 0;
}
